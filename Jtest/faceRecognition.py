import numpy as np
import cv2
import pickle
import time

face_cascade = cv2.CascadeClassifier('cascades/data/haarcascade_frontalface_alt2.xml')

start = time.time()
TIME_TIL_DEATH = 5

# Establishes recognizer and reads data made in faces-train
recognizer = cv2.face_LBPHFaceRecognizer.create()
recognizer.read("trainer.yml")

# loads in the profile ID names
labels = {"person_name": 1}
with open("labels.pickle", 'rb') as f:
    og_labels = pickle.load(f)
    labels = {v:k for k,v in og_labels.items()}

#cap = cv2.VideoCapture(0)
cap = cv2.VideoCapture('C:/Users/Tommy/PycharmProjects/Videos/testVid.mp4')
while True:
    # This goes one frame at a time
    ret, frame = cap.read()

    # converts frame to grayscale - necessary for CV to work

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, scaleFactor=1.5, minNeighbors=5) # mess with these values to increase accuracy
    # iterates through faces
    for(x,y,w,h) in faces:
        print(x, y, w, h) #numbers displayed relay to facial recognition values - no face = numbers stop
        # Provides a REGION OF INTEREST (ROI) value
        roi_gray = gray[y:y+h, x:x+w] # Our ROI has these coordinates (can alter but idk man)\\We are displaying in color but READING in gray hence roi_gray
        roi_color = frame[y:y+h, x:x+w] # Reads roi of color frame (not really necessary but w/e

        # Can alter conf range to dial in what we need.
        # Uses nupy generated matrix of frame taken from video in roi_gray to compare to trained recognizer data
        # NOTE: COMMENT OUT PRINT LINE AFTER ABOVE FOR STATEMENT (currently line 22) TO JUST GET THE PROFILE ID # OF MATCHED FACE
        id_, conf = recognizer.predict(roi_gray)
        if conf >= 45 and conf <= 85:
            print(id_)
            print(labels[id_]) # gives us the actual profile name (folder name)
        img_item = "my-image.png" # saves image
        cv2.imwrite(img_item, roi_gray) # This line and the line above saves an image of JUST the face which is of the coordinates above
        # alter line above to save MULTIPLE images (it just saves one at the moment)

        # This block of code creates a box around our ROI so we can visually monitor our capture
        color = (255, 0, 0) # Blue Green Red 0-255 color of our box
        stroke = 2 # how thicc our line will be
        end_coord_x = x+w
        end_coord_y = y+h
        cv2.rectangle(frame, (x, y), (end_coord_x, end_coord_y), (0, 255, 0), 1)

    # Displays frame
    cv2.imshow('frame', frame)
    # Kills the script/video read if "q" is hit or after 5 seconds.
    # Change TIME_TIL_DEATH value (its in seconds) to make it longer/shorter
    if (time.time() > start + TIME_TIL_DEATH) or (cv2.waitKey(20) & 0xFF == ord('q')):
        break



# Released what was captured after process is completed
cap.release()
cv2.destroyAllWindows()
