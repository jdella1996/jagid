// @author Trevor Amburgey
// @date 2019-10-25
// Please do not modify unless you know what you are doing specifically you Jacob Della

// modules required for server
var express = require('express');
var multer = require('multer')
var path = require('path');
var database = require('./apisupport/dbcfg.js');
var session = require('express-session');
var rimraf = require("rimraf");
var fs = require('node-fs-extra');
var {PythonShell} = require('python-shell');
var bodyParser = require('body-parser');
var dir = __dirname + '/files/';
var app = express();
var node_ssh = require('node-ssh')
var ssh = new node_ssh()







//Special variables
var ServerIP = "192.168.1.182";
var ServerPort = 8080;

var RaspberryIP = "192.168.1.21";
var RaspberryUser = "pi";
var RaspberryPass = "delta";

// grab port as argument or use 7158
console.log("Starting server on IP: " + ServerIP + "\n");
console.log("Starting server on port: " + ServerPort + "\n");



app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
console.log('\nServer spooling up. \n');
//Tests ability to create MySQL session.
//Please do not delete this function
database.InitializeMySQLSession();

//You're gonna get an error from this async function 
ssh.connect({
    host: RaspberryIP,
    username: RaspberryUser,
    password: RaspberryPass,
    //RSA Key is Pi specific.
    //privateKey: fs.readFileSync(dir + 'ssh-key/rpi_rsa')
})



//Express crap
app.use(session({
    secret: 'eccothedolphin',
    resave: true,
    saveUninitialized: true
}));


//Constructs multer path based on account_id
var trainingstorage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './files/' + req.session.account_id + '/training/' + req.session.username)  
    },
    filename: function(req, file, cb){
        cb(null,file.originalname);
    }
});

//Constructs path based on account_id
var trainingupload = multer({ storage: trainingstorage});


// Send your post request to this endpoint to save your file
app.post("/api/trainingupload", trainingupload.single('file'), function(req,res,next){
    let file = req.file
    if (req.session.loggedin) {
        res.send(file)
    } else {
      res.send({ message: 'You are not logged in'});   
    }

});


//Constructs path based on account_id
var verifystorage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './files/' + req.session.account_id + '/verify')
    },
    filename: function(req, file, cb){
        cb(null,file.originalname);
    }
});

var verifyupload = multer({ storage: verifystorage});


//Endpoint for uploading verify data.
//Doesn't do a whole lot.
app.post("/api/verifyupload", verifyupload.single('file'), function(req,res,next){
    let file = req.file
    if (req.session.loggedin){
        res.send(file)
    } else {
      res.send({ message: 'You are not logged in'});   
    }

});

//Just tests ability to spawn a python executable and test the API.
app.get('/api/pythontest', function(req, res) {
    console.log("Python spawned");
    let traindatastorepath = dir + req.session.account_id +'/store';
    let verifypath = dir + req.session.account_id +'/verify';

    var options = {
        mode: 'text',
        pythonOptions: ['-u'],
        scriptPath: './python/testscripts/',
        args: [traindatastorepath, verifypath]
      };

    PythonShell.run('trainer.py', options, function (err, data) {
        if (err) res.send(err);
        //This is just gonna give you guys errors so don't worry about it.
        ssh.execCommand('cd /home/pi/Desktop/jagid/server/python/pi_scripts && python3 accessgranted.py', { cwd:'/var/www' }).then(function(result) {
            console.log(result.stdout);
        });

        res.send(data.toString());
    });
});

//Starts training for user.
app.get('/api/starttraining', function(req, res){
    if(req.session.loggedin){
        let traindatapath = dir+req.session.account_id+'/training/';
        let traindatastorepath = dir + req.session.account_id +"/store";
        let baselinecopy = dir + '/baselines/' + req.session.username;
        //Trainingscript should take two arguements: first one being path to a folder full of photos as an argument.
        //Second one should be the place where we are actually going to store the numpy array.
        //Create a numpy array using said photos to increase confidence level.
        //It won't need to check for the existence of existing training data since we already do that using the database.

        //fs.copy(traindatapath, baselinecopy, function (err) {
         //   if (err) {
          //    console.error(err);
           // } else {
            //  console.log("success!");
           // }
          //}); //copies directory, even if it has subdirectories or files
        
          fs.copy(dir + '/baselines/', traindatapath, function (err) {
            if (err) {
              console.error(err);
            } else {
              console.log("success!");
            }
        }); //copies directory, even if it has subdirectories or files


        var options = {
            mode: 'text',
            pythonOptions: ['-u'],
            scriptPath: './python/',
            args: [traindatastorepath, traindatapath]
        };


        //Python invocation
        PythonShell.run('theTrainer.py', options, function (err, data) {
            if (err)
            {
                console.log(err);
                data = "false";
            }
            if (data.toString() == "false") {
                //If you have a problem training for some reason just return false.
               // rimraf(dir+req.session.account_id+'/training/*',fnunction(){});
               // rimraf(dir+req.session.account_id+'/store/*',function(){});              
                res.send({ message: 'Training failed, something went wrong'});   
            } else {
                database.cfg.query('UPDATE accounts SET account_istrained = ? WHERE account_id = ?', [1, req.session.account_id], function (err, results, fields) {
                    if (err) {
                        //If a SQL error occurs during the training the data and data store are dumped.
                        rimraf(dir+req.session.account_id+'/training/' + req.session.username,function(){});
                        rimraf(dir+req.session.account_id+'/store/*',function(){});     
                        res.send({ message: 'Training bit change failed on API. Server down?'});
                    }
                });
                req.session.loggedin = false;
                req.session.username = '';
                req.session.password = '';
                //rimraf(dir+req.session.account_id+'/training/*',function(){});
                res.send({ message: 'Training complete you have been logged out, please log back in.'});
                rimraf(dir+req.session.account_id+'/training/' + req.session.username,function(){});
            }
        });
    } else {
        res.send({ message: 'You are not logged in.'});
    }

});




//Verifies the user's face based on data stored inside the photo buffer by calling external python scripts.
app.get('/api/verifyface', function(req, res){
    if(req.session.loggedin){
        let traindatastorepath = dir + req.session.account_id +'/store';
        let verifypath = dir + req.session.account_id +'/verify';
        //Verifyfacescript should take two arguements: first one being a folder containing the training data we first created.
        //Second one should be the photos that multer acquired through the android app to begin the face verification.
        //We compare a numpy array to the photo or photos in there. 
        //This program only needs to return a true or false since we are feeding it a user defined path.



        var options = {
            mode: 'text',
            pythonOptions: ['-u'],
            scriptPath: './python/',
            args: [traindatastorepath, verifypath, req.session.username]
        };

        
        PythonShell.run('faceRecognition.py', options, function (err, data) {
            console.log(data);
            if (err)
            {
                console.log(err);
                data = "false";
            }
            //Dump the verify buffer after we're done.
            //rimraf(dir+req.session.account_id+'/verify/*',function(){});
            if (data.toString() == "true") {
                //Sets the pins high on the pi and plays stupid ass sounds.
                ssh.execCommand('cd /home/pi/Desktop/jagid/server/python/pi_scripts && python3 accessgranted.py', { cwd:'/var/www' }).then(function(result) {})
                res.send({ message: 'Face authentication succeeded , verify folder dumped'});
                rimraf(dir+req.session.account_id+'/verify',function(){});
            } else {
                //Just plays stupid ass sounds.
                ssh.execCommand('cd /home/pi/Desktop/jagid/server/python/pi_scripts && python3 accessdenied.py', { cwd:'/var/www' }).then(function(result) {})
                res.send({ message: 'Auth denied, verify folder dumped'});            }
        });
    } else {
        res.send({ message: 'You are not logged in.'});
    }

});







// Insert the provided values into respective fields.
app.get('/api/registernewuser', function(req, res) {
	let firstname = req.param('firstname');
	let lastname = req.param('lastname');
	let username = req.param('username');
	let password = req.param('password');
    let email = req.param('email');
	if (username && password) {
		// Insert the provided values into respective fields.
		database.cfg.query('INSERT INTO accounts (account_username, account_password, account_email, account_firstname, account_lastname) VALUES (? , ?, ?, ?, ?)'
		,[username, password, email, firstname, lastname], function(err, results, fields) {
			if (!err){
				res.send({ message: 'Account successfuly created!'});
				console.log("New user created");
				console.log("Password: " + password);
                console.log("Username: " + username);
                
                database.cfg.query('SELECT account_id FROM accounts WHERE account_username = ?', username, function(err, results){

                    if(results){
                        // update save location
                        let account_id = results[0].account_id;
                        if (!fs.existsSync(dir + account_id)){
                            fs.mkdirSync(dir + account_id);

                        }
                        //res.send({ message: 'Login succesful' });
                    } else {
                        res.send({ message: 'An error has occured creating the account' });
                    }
                });
			} else {
				res.send({ message: 'Please enter valid user name and password' });
			}
		});
	} else {
		//No password
		res.send({ message: 'Please enter Username and Password.'});
	}
});


app.get('/api/auth', function(req, res) {
    let username = req.param('username');
    let password = req.param('password');    
	//Login stuff
	if (username && password) {
		//How you send MySQL queries to the database.
		database.cfg.query('SELECT * FROM accounts WHERE account_username = ? AND account_password = ?', [username, password], function(err, results, fields){
			if (results.length > 0) {
				req.session.loggedin = true;
                req.session.username = username;
                req.session.account_id = results[0].account_id;
                req.session.account_istrained = results[0].account_istrained;
                database.cfg.query('SELECT account_id FROM accounts WHERE account_username = ?', username, function(err, results){
                    if(results){
                        // update save location
                        console.log("Folder creation succeeded for " + username);
                        if (!fs.existsSync(dir +  req.session.account_id + 'training/')){
                            
                            database.cfg.query('UPDATE accounts SET account_istrained = ? WHERE account_id = ?', [0, req.session.account_id], function (err, results, fields) {
                                            if (!err) {
                                                //if (!fs.existsSync(dir + '/baselines/' + req.session.username))
                                                //    fs.mkdirSync(dir + '/baselines/'+ req.session.username);
                                                if (!fs.existsSync(dir +  req.session.account_id))    
                                                    fs.mkdirSync(dir + req.session.account_id);
                                                if (!fs.existsSync(dir +  req.session.account_id + '/training/'))
                                                    fs.mkdirSync(dir + req.session.account_id+'/training');
                                                if (!fs.existsSync(dir +  req.session.account_id + '/training/' + req.session.username))
                                                    fs.mkdirSync(dir + req.session.account_id+'/training/' + req.session.username);
                                                if (!fs.existsSync(dir +  req.session.account_id + '/store/'))
                                                    fs.mkdirSync(dir + req.session.account_id+'/store');
                                                if (!fs.existsSync(dir +  req.session.account_id + '/verify/'))
                                                    fs.mkdirSync(dir + req.session.account_id+'/verify');
                                                res.send({message : "Login succesful, Data dumped for user with ID :" + req.session.account_id});
                                            } 
            
                                });
                            //Generates folders.

                    } 
                    else {
                            res.send({ message: 'Login succesful' });
                        }

                    }
                });

            } 
            else {
				res.send({ message: 'Error, invalid login' });
			}			
		});
    }
    else {
		//No password
		res.send({ message: 'Please enter a user name and password' });
    }
});


app.get('/api/dumptrainingdata', function(req, res){
    if(req.session.loggedin){
        req.session.account_istrained = 0;
        database.cfg.query('SELECT account_id FROM accounts WHERE account_username = ?', req.session.username, function(err, results){
            if(results){
                let account_id = results[0].account_id;
                rimraf(dir+account_id+'/training/*',function(){});
                rimraf(dir+account_id+'/store/*',function(){});
                rimraf(dir+account_id+'/verify/*',function(){});
                database.cfg.query('UPDATE accounts SET account_istrained = ? WHERE account_id = ?', [0, account_id], function (err, results, fields) {
                                if (!err) {
                                    req.session.loggedin = false;
                                    req.session.username = '';
                                    req.session.password = '';
                                    res.send({message : "Logging off, dumped for user with ID :" + account_id});
                                } else {
                                    res.send({message : "Something went wrong :("});
                                }

            });   
            
         }
        });
    }
});

//Are you logged in?
app.post('/api/isloggedin', function(req, res) {
    if (req.session.loggedin) {
		res.send({ loggedin: true});
	} else {
		res.send({ loggedin: false});
	} 	
});

//Whoami
app.get('/api/whoami', function(req, res) {
	if (req.session.loggedin) {
		res.send({username: req.session.username, accountid : accountid});
    } else {
        res.send({message : "You aren't logged in"});
    }
});

//Logs you out.
app.get('/api/logout', function(req, res) {
	req.session.loggedin = false;
	req.session.username = '';
	req.session.password = '';
	res.send({ message: 'You have been logged out'});	
});
 


// listen for requests on the given port
app.listen(ServerPort);
