import subprocess
import time
import RPi.GPIO as GPIO
import os
from audio import *

# GPIO setup
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(18,GPIO.OUT)

# Sets pin 18 to a high state than to a low state.
GPIO.output(18,GPIO.HIGH)
print("LED is on")
developeraccessgrantedsound()
time.sleep(10)
GPIO.output(18,GPIO.LOW)
print("LED is off")
sys.stdout.flush()
exit()
